//
//  ViewController.swift
//  Accordion
//
//  Created by Sayeed Munawar on 10/6/18.
//  Copyright © 2018 Sayeed Munawar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private let viewModel = ViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "CellIdentifier"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) ?? UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
        cell.textLabel?.text = viewModel.title(at: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.selectRow(at: indexPath.row, deletion: { indexPathsToDelete in
            tableView.beginUpdates()
            tableView.deleteRows(at: indexPathsToDelete, with: .top)
            tableView.endUpdates()
        }, insertion: { indexPathsToAdd in
            tableView.beginUpdates()
            tableView.insertRows(at: indexPathsToAdd, with: .top)
            tableView.endUpdates()
        })
    }
}

