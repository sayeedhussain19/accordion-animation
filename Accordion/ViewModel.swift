
//  ViewModel.swift
//  Accordion
//
//  Created by Sayeed Munawar on 10/6/18.
//  Copyright © 2018 Sayeed Munawar. All rights reserved.
//


import UIKit

class ViewModel {
    
    static private let Countries = ["A", "B", "C", "D"]
    static private let Cities = [
                "A" : ["    AA","    AB","    AC","    AD","    AE","    AF","    AG","    AH","    AI","    AJ","    AK","    AL","    AM","    AN","    AO","    AP","    AQ","    AR","    AS","    AT","    AU","    AV","    AW","    AX","    AY","    AZ"],
                "B" : ["    BA","    BB","    BC","    BD","    BE","    BF","    BG","    BH","    BI","    BJ","    BK","    BL","    BM","    BN","    BO","    BP","    BQ","    BR","    BS","    BT","    BU","    BV","    BW","    BX","    BY","    BZ"],
                "C" : ["    CA","    CB","    CC","    CD","    CE","    CF","    CG","    CH","    CI","    CJ","    CK","    CL","    CM","    CN","    CO","    CP","    CQ","    CR","    CS","    CT","    CU","    CV","    CW","    CX","    CY","    CZ"],
                "D" : ["    DA","    DB","    DC","    DD","    DE","    DF","    DG","    DH","    DI","    DJ","    DK","    DL","    DM","    DN","    DO","    DP","    DQ","    DR","    DS","    DT","    DU","    DV","    DW","    DX","    DY","    DZ"]
    ]
    
    private let countries: [String]
    private let cities: [String: [String]]
    private var selectedCountry: String? = nil
    private var viewDataSource: [String]
    
    private var selectedCountryCities : [String] {
        if let country = selectedCountry, let cities = cities[country] {
            return cities
        }
        return []
    }
    
    private var indexOfSelectedCountry: Int? {
        if let selectedCountry = selectedCountry {
            return countries.index(of: selectedCountry)!
        }
        return nil
    }

    var numberOfRows: Int {
        return viewDataSource.count
    }

    init(countries: [String] = ViewModel.Countries, cities: [String: [String]] = ViewModel.Cities) {
        self.countries = countries
        self.cities = cities
        self.viewDataSource = countries
    }
    
    func selectRow(at index: Int, deletion:([IndexPath]) -> (), insertion:([IndexPath]) -> ()) {
        if index == indexOfSelectedCountry || isIndexCity(index: index) {
            return
        }
        
        var indexPathsToDelete = [IndexPath]()
        
        if let indexOfSelectedCountry = indexOfSelectedCountry {
            for i in 1...selectedCountryCities.count {
                indexPathsToDelete.append(IndexPath(row: indexOfSelectedCountry+i, section: 0))
            }
        }
        
        if indexPathsToDelete.count > 0 {
            viewDataSource = countries
            deletion(indexPathsToDelete)
        }
        
        let adjustedIndex = adjustedIndexFor(index: index)
        selectedCountry = countries[adjustedIndex]
        viewDataSource = countries[0...adjustedIndex] + selectedCountryCities + (adjustedIndex == countries.count - 1 ? [] : countries[adjustedIndex+1..<countries.count])
        
        var indexPathsToAdd = [IndexPath]()

        if let indexOfSelectedCountry = indexOfSelectedCountry {
            for i in 1...selectedCountryCities.count {
                indexPathsToAdd.append(IndexPath(row: indexOfSelectedCountry+i, section: 0))
            }
        }

        if indexPathsToAdd.count > 0 {
            insertion(indexPathsToAdd)
        }
    }
    
    func title(at index: Int) -> String {
        assert(index >= 0 && index < viewDataSource.count)
        return viewDataSource[index]
    }
    
    //MARK: - Private
    
    private func isIndexCity(index: Int) -> Bool {
        
        if indexOfSelectedCountry == nil {
            return false
        }
        
        let selectedCountryIndex = countries.index(of: selectedCountry!)!
        return index > selectedCountryIndex && index <= selectedCountryIndex + selectedCountryCities.count
    }

    private func adjustedIndexFor(index: Int) -> Int {
        
        guard let indexOfSelectedCountry = indexOfSelectedCountry else {
            return index
        }

        if index <= indexOfSelectedCountry {
            return index
        }

        return index - selectedCountryCities.count
    }

}
