//
//  ViewModelTests.swift
//  AccordionTests
//
//  Created by Sayeed Munawar on 10/6/18.
//  Copyright © 2018 Sayeed Munawar. All rights reserved.
//

import XCTest

@testable import Accordion

class ViewModelTests: XCTestCase {
    
    func testNumberOfRowsWhenNoCountryIsSelected() {
        let viewModel = ViewModel(countries: ["A", "B"], cities: [:])
        XCTAssert(viewModel.numberOfRows == 2)
    }
    
    func testNumberOfRowsWhenCountryWithCitiesIsSelected() {
        let viewModel = ViewModel(countries: ["A", "B"], cities: ["A":["AA","AB"]])
        viewModel.selectRowAt(at: 0)
        XCTAssert(viewModel.numberOfRows == 4)
    }

    func testNumberOfRowsWhenCountryWithoutCitiesIsSelected() {
        let viewModel = ViewModel(countries: ["A", "B"], cities: ["A":["AA","AB"]])
        viewModel.selectRowAt(at: 1)
        XCTAssert(viewModel.numberOfRows == 2)
    }

    func testTitleAtWhenNoCountryIsSelected() {
        let viewModel = ViewModel(countries: ["A", "B"], cities: ["A":["AA","AB","AC"], "B":["BA"]])
        XCTAssertEqual("B", viewModel.title(at: 1))
    }

    func testTitleAtWhenCountryWithCityIsSelected() {
        let viewModel = ViewModel(countries: ["A", "B"], cities: ["A":["AA","AB"]])
        viewModel.selectRowAt(at: 0)
        XCTAssertEqual("AB", viewModel.title(at: 2))
    }

    func testTitleAtWhenCountryWithoutCitiesIsSelected() {
        let viewModel = ViewModel(countries: ["A", "B"], cities: ["A":["AA","AB"]])
        viewModel.selectRowAt(at: 1)
        XCTAssertEqual("B", viewModel.title(at: 1))
    }
    
    func testSelectRowWhichHasCountryReturnsTrue() {
        let viewModel = ViewModel(countries: ["A", "B"], cities: ["A":["AA","AB","AC"]])
        let needsReload = viewModel.selectRowAt(at: 0)
        XCTAssert(viewModel.numberOfRows == 5)
        XCTAssertTrue(needsReload)
    }

    func testSelectRowWhichHasCountryThatIsCurrentlySelectedReturnsFalse() {
        let viewModel = ViewModel(countries: ["A", "B"], cities: ["A":["AA","AB","AC"]])
        let _ = viewModel.selectRowAt(at: 0)
        let needsReload = viewModel.selectRowAt(at: 0)
        XCTAssertFalse(needsReload)
    }

    func testSelectRowWhichHasCityDoesNotReturnTrue() {
        let viewModel = ViewModel(countries: ["A", "B"], cities: ["A":["AA","AB","AC"]])
        let _ = viewModel.selectRowAt(at: 0)
        let needsReload = viewModel.selectRowAt(at: 2)
        XCTAssertFalse(needsReload)
    }

    func testSelectRowWhichHasCountryAfterSelectingACountryAtIndexGreaterThanCurrentSelectedRowReturnsTrue() {
        let viewModel = ViewModel(countries: ["A", "B"], cities: ["A":["AA","AB"],"B":["BA","BB","BC"]])
        let _ = viewModel.selectRowAt(at: 1)
        let needsReload = viewModel.selectRowAt(at: 0)
        XCTAssertEqual("AB", viewModel.title(at: 2))
        XCTAssertTrue(needsReload)
    }
    
    func testSelectRowWhichHasCountryAfterSelectingACountryAtIndexLesserThanCurrentSelectedRowReturnsTrue() {
        let viewModel = ViewModel(countries: ["A", "B"], cities: ["A":["AA","AB"],"B":["BA","BB","BC"]])
        let _ = viewModel.selectRowAt(at: 0)
        let needsReload = viewModel.selectRowAt(at: 3)
        XCTAssertEqual("BA", viewModel.title(at: 2))
        XCTAssertTrue(needsReload)
    }

}
